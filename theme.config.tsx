import React from 'react';
import { DocsThemeConfig } from 'nextra-theme-docs';

const config: DocsThemeConfig = {
  logo: <span>Developer roadmap</span>,
  docsRepositoryBase: 'https://gitlab.com/huyxvd/developer-roadmap/-/tree/master/',
  footer: {
    text: 'Developer roadmap',
  },

  useNextSeoProps: () => {
    return {
      titleTemplate: '%s – Dev Road'
    }
  },
  banner: {
    key: '4.0-release',
    text: (
      <a href="https://forms.gle/QDEd89x8diNAaUQe9" target="_blank">
        🎉 Lớp học C# .NET online. Tìm hiểu ngay →
      </a>
    )
  }
}

export default config
